


module.exports = {
    "extends": [
        "react-app",
        "react-app/jest"
    ],
    "rules": {//开发环境能使用console.log();生产环境不能用
        "no-console": process.env.REACT_APP_MODE === 'development' ? 'off' : "error",//不能使用error,
        "no-unused-vars": 'error'
    }
}