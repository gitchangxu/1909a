
const CracoLessPlugin = require('craco-less');//支持less
const path = require('path')
//全局输入less变量文件
const cracoLessResourcesLoader = require('craco-plugin-style-resources-loader')
module.exports = {
    webpack: {
        alias: {
            "@": path.join(__dirname, 'src'),
            "coms": path.join(__dirname, 'src/components'),
            "api": path.join(__dirname, 'src/api'),
        },
    },
    plugins: [
        {
            plugin: CracoLessPlugin,
            options: {
                lessLoaderOptions: {
                    lessOptions: {
                        modifyVars: { '@primary-color': '#1DA57A' },
                        javascriptEnabled: true,
                    },
                },
            },
        },
        {
            plugin: cracoLessResourcesLoader,
            options: {
                patterns: path.join(__dirname, 'src/style/var.less'),
                styleType: 'less'
            }
        }
    ],

}